# README #
### This repository ###

* Holds source code of the arduino microcontroller powering development/ prototype of warming wiz
* PCB design layout designed using AutoCAD Eagle

* Version: v.0.6.0

### Contribution guidelines ###

* Software: Daniel A. Lerma
* Hardware: Daniel A. Lerma

### Who do I talk to? ###

* Daniel A. Lerma
* dani.lerma.d4@gmail.com