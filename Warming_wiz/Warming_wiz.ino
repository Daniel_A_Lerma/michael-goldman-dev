
/* WARMING WIZ DEVELOPMENT - WHAT IT DOES: 
   - Receives characters from Serial Monitor
   - controls current flow through transistor
   - operates at a configurable timer
 - CONNECTIONS:
   - None: Pin 9 transistor
   - 
 - Version: 0.0.1 03/09/17
   Questions: dani.lerma.d4@gmail.com */
   
 //--- includes bellow ----
 #include <avr/sleep.h>  

 //--- variables bellow ---
 const int transistorPin = 9;    // connected to the base of the transistor
 int ByteReceived;
 
 void setup() {
   // set  the transistor pin as output:
   pinMode(transistorPin, OUTPUT);
   // monitor
   Serial.begin(9600);  
   Serial.println("--------------------------------------");
   Serial.println("--- Start Serial Monitor SEND_RCVE ---");
   Serial.println("-- WARMING WIZ SERIAL DEBUG V.0.0.1 --");
   Serial.println("--------------------------------------");
   //Serial.println("(Decimal)(Hex)(Character)");  
   Serial.println("Character Recieved         Message");  
   Serial.println(); 
 }

 void loop() {
//-----------------------------------------------    
//--------------------------TIMING CODE----------    
//----------------------------------------------- 
 const  unsigned long Minutes = 10 * 60 * 1000UL;
        unsigned long now = millis();
        
    //run for a set number of time
    if (now == Minutes){
      analogWrite(transistorPin, 0);     
      cli();
      sleep_enable();
      sleep_cpu();
    }
//-----------------------------------------------    
//-----------------TRANSISTOR CONTROL CODE-------    
//-----------------------------------------------
    
 // read the potentiometer:
 int sensorValue = analogRead(A0);
 
 // map the sensor value to a range from 0 - 255:
 int outputValue = map(sensorValue, 0, 1023, 0, 255);
 
 // control the transistor:
 analogWrite(transistorPin, outputValue); 
    
//-----------------------------------------------    
//-----------------SERIAL DEBUGING CODE----------    
//-----------------------------------------------    
  if (Serial.available() > 0)
  {
    ByteReceived = Serial.read();
    //Serial.print(ByteReceived);   
    //Serial.print(ByteReceived, HEX);
    Serial.print(char(ByteReceived));
    
    if(ByteReceived == '1'){
        //print timer count
        Serial.print(outputValue);
        Serial.print(" Pot outputValue");
    }
    
    if(ByteReceived == '2'){
        //print time
        Serial.print(now);
        Serial.print(" time now");
    }

    if(ByteReceived == '3'){
        //print 
        Serial.print(Minutes);
        Serial.print(" minutes setting");
    }
   
    Serial.println();    // End the line
  // END Serial Available
  }  
//-----------------------------------------------    
//-----------------------------------------------    
//-----------------------------------------------    
}
